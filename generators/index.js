const Generator = require('yeoman-generator');

const {isValidDirName, getStyledError, getStyledInfo} = require('./helpers');

const {DEPENDENCIES_LIST, DEV_DEPENDENCIES_LIST } = require('./variables');

const wrongDirectoryNameError = getStyledError(
    'Directory name may contains only range of {a-z} symbols and "-" symbol as words divider'
);

const finalMessage = projectDirName => getStyledInfo([
    'Everything is done.',
    `Now you may change your working directory to \`${projectDirName}\`.`,
    'Then you may type `npm run dev` command to run your blank project.',
].join('\n'));

const gitInitMessage = getStyledInfo([
    'Empty git repository initialized.',
    'Use `git remote add origin PATH-TO-YOUR-REPO` command (https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes)',
].join('\n'));

module.exports = class extends Generator {
    constructor(args, opts) {
        super(args, opts);

        this.argument('dir', {
            required: true,
            type: String,
            desc: 'Folder to unfold the project'
        });

        const [targetDirName] = this.args;

        this.targetDirName = targetDirName;
        this.cwdConfig = {cwd: targetDirName};

        if (!isValidDirName(targetDirName)) {
            throw new Error(wrongDirectoryNameError);
        }
    }
    _protected_gitInitEmptyRepo() {
        this.spawnCommandSync(
            'git',
            ['init'],
            this.cwdConfig,
        );
        this.log(gitInitMessage);
    };
    writing() {
        const appTemplateFilesPath = this.templatePath() + '/../app/**/*.*';
        const appTemplateHiddenFilesPath = this.templatePath() + '/../app/**/.*';
        const projectFolderPath = this.destinationPath() + '/' + this.targetDirName;
        this.fs.copyTpl(appTemplateFilesPath, projectFolderPath, {targetDirName : this.targetDirName});
        this.fs.copyTpl(appTemplateHiddenFilesPath, projectFolderPath);
    }

    install() {
        this.npmInstall(DEPENDENCIES_LIST, {'save': true}, this.cwdConfig);
        this.npmInstall(DEV_DEPENDENCIES_LIST, {'save-dev': true}, this.cwdConfig);
    }

    end() {
        this._protected_gitInitEmptyRepo();
        this.log(finalMessage(this.targetDirName));
    }
};
