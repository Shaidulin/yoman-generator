import React from 'react';
import Document, { Head, Main, NextScript } from 'next/document';

export default class MyDocument extends Document {
    render() {
        return (
            <html lang="ru">
                <Head>
                    <meta
                        name="viewport"
                        content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=yes"
                    />
                    {/* <link rel="shortcut icon" href="../static/images/favicon.ico" type="image/x-icon"/> */}
                    {/* <link rel="icon" href="../static/images/favicon.ico" type="image/x-icon"/> */}
                    {/* {this.props.styleTags} */}
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </html>
        );
    }
}
