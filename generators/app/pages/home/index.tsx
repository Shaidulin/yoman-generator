import React from 'react';
import { NextPage } from 'next';
import { IHomePage } from '@interfaces';

import Container from './components/container';
import Apod from './components/apod';
import ApodButton from './components/apod-button';
import Middle from './components/middle';
import MiddleLeft from './components/middle-left';
import MiddleLeftButtons from './components/middle-left-buttons';
import MiddleRight from './components/middle-right';
import Top from './components/top';
import TopText from './components/top-text';

const Home: NextPage<IHomePage.IProps, IHomePage.InitialProps> = () => {
    return (
        <Container>
            <Top />
            <Middle>
                <MiddleLeft>
                    <MiddleLeftButtons>Click me</MiddleLeftButtons>
                </MiddleLeft>
                <MiddleRight>
                    <TopText>Hello</TopText>
                    <Apod>
                        <ApodButton>Discover Space8</ApodButton>
                    </Apod>
                </MiddleRight>
            </Middle>
        </Container>
    );
};

export default Home;
