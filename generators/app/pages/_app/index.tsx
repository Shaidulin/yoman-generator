import * as React from 'react';
import App from 'next/app';
import { ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';

import { theme } from '@theme';
import { AppWithStore } from '@interfaces';
import { makeStore } from '@store';

import '@static/css/reset.css';

const store = makeStore({ counter: 'test' });

class WebApp extends App<AppWithStore> {
    render() {
        const { Component, pageProps } = this.props;

        return (
            <Provider store={store}>
                <ThemeProvider theme={theme}>
                    {/* eslint-disable-next-line react/jsx-props-no-spreading */}
                    <Component {...pageProps} />
                </ThemeProvider>
            </Provider>
        );
    }
}

export default WebApp;
