import React from 'react';
import Home from '@pages/home';

import Link from 'next/link';

const Layout = () => (
    <>
        <Link href="/home">
            <div>Default page</div>
        </Link>
        <Home />
    </>
);

export default Layout;
