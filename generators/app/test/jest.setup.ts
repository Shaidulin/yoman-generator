import dotenv from "dotenv";
import enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import "jest-styled-components";
import { setConfig } from "next/config";

dotenv.config({ path: ".env.test" });

setConfig({
    publicRuntimeConfig: {
        API_URL: process.env.API_URL
    },
});

enzyme.configure({ adapter: new Adapter() });

dotenv.config({ path: ".env.test" });
