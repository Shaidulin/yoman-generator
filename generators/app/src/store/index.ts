import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk';

import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { HomeReducer } from '@store/home';

const reducers = combineReducers({
    home: HomeReducer,
});

export const makeStore = (initialState: {}) => {
    return createStore(
        reducers,
        initialState,
        composeWithDevTools(applyMiddleware(thunkMiddleware))
    );
};
