export interface IMapPayload {}

export interface IStateProps {
    home: {
        version: number;
    };
    image: {
        url: string;
    };
}
