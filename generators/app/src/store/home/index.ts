import { IMapPayload, IStateProps } from '@store/home/interfaces/IStore';
import { IAction } from '@store/home/interfaces/IAction';

export const INITIAL_STATE: IStateProps = {
    home: {
        version: 1,
    },
    image: {
        url: '',
    },
};
export const SET = 'SET';
export const RESET = 'RESET';

export const HomeReducer = (
    state = INITIAL_STATE,
    action: IAction<IMapPayload>
) => {
    switch (action.type) {
        case SET:
            return {
                ...state,
                ...action.payload,
            };

        case RESET:
            return INITIAL_STATE;

        default:
            return state;
    }
};
