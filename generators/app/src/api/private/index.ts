import getConfig from 'next/config';

import { AxiosApi } from '@api/axios';

const privateConfig = {
    headers: { 'X-Requested-With': 'XMLHttpRequest' },
    withCredentials: true,
};

const {
    publicRuntimeConfig: { API_URL },
} = getConfig();

export const http = new AxiosApi(privateConfig, API_URL);
