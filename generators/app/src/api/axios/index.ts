import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';

export class AxiosApi {
    private api: AxiosInstance;

    private baseUrl: string;

    public constructor(config: AxiosRequestConfig, url: string) {
        this.api = axios.create(config);
        this.api.interceptors.request.use((param: AxiosRequestConfig) => ({
            ...param,
        }));

        this.baseUrl = url;
    }

    public get<T, R = AxiosResponse<T>>(
        url: string,
        config?: AxiosRequestConfig
    ): Promise<R> {
        return this.api.get(this.baseUrl + url, config);
    }

    public delete<T, R = AxiosResponse<T>>(
        url: string,
        config?: AxiosRequestConfig
    ): Promise<R> {
        return this.api.delete(this.baseUrl + url, config);
    }

    public head<T, R = AxiosResponse<T>>(
        url: string,
        config?: AxiosRequestConfig
    ): Promise<R> {
        return this.api.head(this.baseUrl + url, config);
    }

    public post<T, R = AxiosResponse<T>>(
        url: string,
        data?: string,
        config?: AxiosRequestConfig
    ): Promise<R> {
        return this.api.post(this.baseUrl + url, data, config);
    }

    public put<T, R = AxiosResponse<T>>(
        url: string,
        data?: string,
        config?: AxiosRequestConfig
    ): Promise<R> {
        return this.api.put(this.baseUrl + url, data, config);
    }

    public patch<T, R = AxiosResponse<T>>(
        url: string,
        data?: string,
        config?: AxiosRequestConfig
    ): Promise<R> {
        return this.api.patch(this.baseUrl + url, data, config);
    }
}
