import { Store } from 'redux';
import { AppInitialProps } from 'next/app';
import { ThunkDispatch } from 'redux-thunk';

interface AppStore extends Store {
    dispatch: ThunkDispatch;
}

export interface AppWithStore extends AppInitialProps {
    store: AppStore;
}
