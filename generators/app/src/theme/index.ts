import { DefaultTheme } from 'styled-components';

const theme: DefaultTheme = {
    colors: {
        primary: '#2c3e50',
    },
};

export { theme };
