const withPlugins = require("next-compose-plugins");
const withCSS = require("@zeit/next-css");
const withBundleAnalyzer = require("@zeit/next-bundle-analyzer");
const withImages = require("next-images");
const nextRuntimeDotenv = require('next-runtime-dotenv');

const withConfig = nextRuntimeDotenv({ public: ["API_URL", "API_KEY"] });

const configNext = {
    cssModules: true,
    cssLoaderOptions: {
        importLoaders: 1,
        localIdentName: "[local]___[hash:base64:5]",
    },
    publicRuntimeConfig: {
        API_URL: process.env.API_URL,
    },
};

module.exports = withConfig(withPlugins(
    [withCSS, withBundleAnalyzer, withImages],
    configNext
));
