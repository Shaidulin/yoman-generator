const chalk = require('chalk');

/**
 * @param {string} dirName
 * @return {boolean}
 */

const isValidDirName = dirName => /^[a-z](:?[a-z\-]+[a-z])?$/.test(dirName);

/**
 *
 * @param {Object} styleOptions
 * @return {function(string): string}
 */

const styleText = (styleOptions = []) => text => styleOptions.reduce(
    (styledErrorText, option) => chalk[option](styledErrorText), text
);

const getStyledError = styleText(['bold', 'white', 'bgRed']);
const getStyledInfo = styleText(['bold', 'white']);

module.exports = {
    isValidDirName,
    getStyledError,
    getStyledInfo,
};
